const sass = require('node-sass');

const scripts = {
    vendors: [
        'node_modules/jquery/dist/jquery.slim.min.js',
        'node_modules/popper.js/dist/umd/popper.min.js',
        'node_modules/bootstrap/dist/js/bootstrap.min.js',
        'node_modules/owl.carousel/dist/owl.carousel.min.js',
    ],
    modules: [
        'src/js/module-tabs.js',
        'src/js/module-carousel.js',
        'src/js/main.js'
    ]
}


module.exports = function (grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        sass: {
            dev: {
                options: {
                    implementation: sass
                },
                files: {
                    'assets/css/style.css': 'src/scss/style.scss'
                }
            },

            dist: {
                options: {
                    implementation: sass
                },
                files: {
                    'assets/css/style.css': 'src/scss/style.scss'
                }
            }
        },

        concat: {
            vendors: {
                src: scripts.vendors,
                dest: 'assets/js/vendor.min.js'
            },
            dev: {
                src: scripts.modules,
                dest: 'assets/js/scripts.min.js'
            },
            dist: {
                options: {
                    separator: ';',
                },
                src: scripts.modules,
                dest: 'assets/js/scripts.min.js'
            }
        },

        uglify: {
            dev : {
                options : {
                    beautify : false,
                    preserveComments : true,
                    sourceMap : true,
                    mangle : true
                },
                files : {
                    "assets/js/scripts.min.js" : "assets/js/scripts.min.js"
                }
            },

            dist : {
                options : {
                    beautify : true,
                    preserveComments : false,
                    sourceMap : false,
                    mangle : true
                },
                files : {
                    "assets/js/scripts.min.js" : "assets/js/scripts.min.js"
                }
            },

        },

        watch: {
            sass: {
                files: ['sass/*.scss'],
                tasks: ['sass', 'cssmin']
            }
        },

    });


    // Load the plugin that provides the "sass" task.
    grunt.loadNpmTasks('grunt-sass');

    // Load the plugin that provides the "uglify" task.
    grunt.loadNpmTasks('grunt-contrib-uglify');

    // Load the plugin that provides the "concat" task.
    grunt.loadNpmTasks('grunt-contrib-concat');


    // Default task(s).
    grunt.registerTask('default', ['dev']);

    // Dev task(s)
    grunt.registerTask('dev', ['sass:dev', 'concat:vendors', 'concat:dev']);

    // Dist task(s)
    grunt.registerTask('dist', ['sass:dist', 'concat:vendors', 'concat:dist']);
}

function setupTabs(){
    document.querySelectorAll('.tab-btn').forEach(button => button.addEventListener('click', function(){
        const menu = button.parentElement;
        const tabsContainer = menu.parentElement;
        const tabNumber = button.dataset.forTab;
        const tabToActivate = tabsContainer.querySelector(`.tab-panel[data-tab="${tabNumber}"]`);

        /* console.log(menu);
        console.log(tabsContainer);
        console.log(tabNumber);
        console.log(tabToActivate); */

        menu.querySelectorAll('.tab-btn').forEach(button =>{
            button.classList.remove('btn-active');
        });

        tabsContainer.querySelectorAll('.tab-panel').forEach(tab =>{
            tab.classList.remove('panel-active')
        });

        button.classList.add('btn-active');

        tabToActivate.classList.add('panel-active');

    }))
}

document.addEventListener("DOMContentLoaded", () =>{
    setupTabs();
    document.querySelectorAll('.header-section').forEach(tabsContainer =>{
        tabsContainer.querySelector('.myMenu .tab-btn').click();
    })
})

$(document).ready(function(){
    $('.owl-carousel').owlCarousel({
        margin:30,
        loop: true,
        nav:true,
            navText: [
                "<span class='nav-button owl-prev fa fa-caret-left'>&#8666;</span>",
                "<span class='nav-button owl-next fa fa-caret-right'> &#8667; </span>"
            ],
        autoplay: true,
        autoplayHoverPause: true,
        slideBy:4,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 2
            },
            900: {
                items: 4
            }
        }
    });
});
